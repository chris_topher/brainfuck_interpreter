# README #

Brainfuck interpreter in Java

### What is this repository for? ###

This repository stores code for a brainfuck interpreter I had to develop for a computing class at Chung Ang University.

### Notice ###
From now on, this interpreter can only handle one loop at a time: No embedded loops are allowed, e.g.:

```
#!brainfuck
This will be undefined behaviour
>>>+++[++>>++++[+++><-]<<-]
```

This, therefore, will work:
```
#!brainfuck
This is more inclined to work
>>>+++[++>>++++<<-]+++[+++>>>]
```


### How to use it ###
Pass a BF file

```
#!bash

./BF "bfcodefile.bf"
```
Pass a BF code directly
```
#!bash

./BF ">>>+++[++>>++++<<-]+++[+++>>>]"
```
For License, see LICENSE.md.