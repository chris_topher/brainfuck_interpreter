package kr.co.cau.brainfuck_interpreter;

import kr.co.cau.brainfuck_interpreter.exceptions.RuntimeException;
import kr.co.cau.brainfuck_interpreter.exceptions.SyntaxException;
import kr.co.cau.brainfuck_interpreter.parser.SyntaxAnalyzer;
import kr.co.cau.brainfuck_interpreter.runners.InstructionsRunner;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Main {

    public static boolean isBFFile(String fileName)
    {
        if (fileName.endsWith(".bf"))
            return true;
        return false;
    }
    static String getFileContent(String path, Charset encoding)
            throws IOException
    {
        byte[] encoded = Files.readAllBytes(Paths.get(path));
        return new String(encoded, encoding);
    }

    public static void main(String[] args) {
        try {
            InstructionsRunner instructionsRunner = new InstructionsRunner();
            String bfInstructions;
            String argument = args[0];
            if (argument == null)
                throw new RuntimeException("No argument found.");
            else if (isBFFile(argument))
                bfInstructions = getFileContent(argument, StandardCharsets.UTF_8);
            else
                bfInstructions = argument;
            if (!SyntaxAnalyzer.areInstructionsValid(bfInstructions))
                throw new SyntaxException("Not matching parenthesis.");
            instructionsRunner.run(bfInstructions);
        }
        catch (Exception e)
        {
            //e.printStackTrace();
            System.err.println(e.getMessage());
        }
    }
}
