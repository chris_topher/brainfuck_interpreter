package kr.co.cau.brainfuck_interpreter.exceptions;

/**
 * Created by Christophine on 28/09/2016.
 */
public class RuntimeException extends Exception {
    public RuntimeException(String message)
    {
        super(RuntimeException.class + ": " + message);
    }
}
