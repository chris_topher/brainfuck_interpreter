package kr.co.cau.brainfuck_interpreter.exceptions;

/**
 * Created by Christophine on 28/09/2016.
 */
public class SyntaxException extends java.lang.Exception {
    public SyntaxException(String message)
    {
        super(SyntaxException.class + ": " + message);
    }
}
