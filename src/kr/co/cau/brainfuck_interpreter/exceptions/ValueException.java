package kr.co.cau.brainfuck_interpreter.exceptions;

/**
 * Created by Christophine on 28/09/2016.
 */
public class ValueException extends Exception {
    public ValueException(String message)
    {
        super(ValueException.class + ": " + message);
    }
}
