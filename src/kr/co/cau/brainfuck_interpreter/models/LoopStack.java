package kr.co.cau.brainfuck_interpreter.models;

import java.util.Stack;

/**
 * Created by Christophine on 30/09/2016.
 */
public class LoopStack {
    private short loopLevel;
    private static LoopStack ourInstance = new LoopStack();

    public static LoopStack getInstance() {
        return ourInstance;
    }
    protected Stack<Integer> stack;
    private LoopStack() {
        loopLevel = 0;
        stack = new Stack<>();
    }
    public void add(int pointerPosition)
    {
        stack.add(pointerPosition);
    }
    public void pop()
    {
        stack.pop();
    }
    public int top()
    {
        return stack.peek();
    }
    public boolean isEmpty()
    {
        return stack.isEmpty();
    }
}
