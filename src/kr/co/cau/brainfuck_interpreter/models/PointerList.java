package kr.co.cau.brainfuck_interpreter.models;

import kr.co.cau.brainfuck_interpreter.exceptions.RuntimeException;
import kr.co.cau.brainfuck_interpreter.exceptions.ValueException;
import kr.co.cau.brainfuck_interpreter.utils.Utils;

import java.util.LinkedList;

/**
 * Created by Christophine on 28/09/2016.
 */

public class PointerList {
    private LinkedList<Integer> list;
    private int position;
    private static PointerList ourInstance = new PointerList();

    public static PointerList getInstance() {
        return ourInstance;
    }

    private PointerList() {
        this.list = new LinkedList<>();
        this.list.add(0);
        this.list.add(0);
        this.list.add(0);
        this.list.add(0);
        position = 0;
    }
    public void moveNext() throws RuntimeException
    {
        if ((position+ 1) + 1 <= 0)
            throw new RuntimeException("Error: Cannot assign not positive position to pointer.");
        allocateIfNeeded(position);
        position = position + 1;
    }
    public void movePrevious() throws RuntimeException
    {
        if ((position + 1) - 1 <= 0)
            throw new RuntimeException("Error: Cannot assign not positive position to pointer.");
        allocateIfNeeded(position);
        position = position - 1;
    }
    public void allocateIfNeeded(int newPosition)
    {
        if (newPosition + 1 > list.size())
            for (int i = 0; i < newPosition; i++) {
                list.add(0);
            }
    }
    public void     decreaseValue() throws ValueException
    {
        allocateIfNeeded(position);
        int newValue = this.list.get(position) - 1;
        if (!Utils.isAscii(newValue))
            throw  new ValueException(String.format("Error: Cannot assign non-ASCII value %d to pointer %d.", newValue, position + 1));
        list.set(position, newValue);
    }
    public void increaseValue() throws ValueException
    {
        allocateIfNeeded(position);
        int newValue = this.list.get(position) + 1;
        if (!Utils.isAscii(newValue))
            throw  new ValueException(String.format("Error: Cannot assign non-ASCII value %d to pointer %d.", newValue, position + 1));
        list.set(position, newValue);
    }
    public int getValue()
    {
        allocateIfNeeded(position);
        return (this.list.get(position));
    }
    public int getPosition()
    {
        return position;
    }
    public int getValue(int at)
    {
        allocateIfNeeded(at);
        return (this.list.get(at));
    }
}
