package kr.co.cau.brainfuck_interpreter.parser;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Christophine on 28/09/2016.
 */
public class LexicalAnalyzer {
    protected static final List<Character> lexer = Arrays.asList('+', '-',
                '>', '<', '.', '[', ']');
    public static boolean isToken(char token)
    {
        return lexer.contains(token);
    }

    public static boolean isOpeningLoop(char token) {
        return token == '[';
    }

}
