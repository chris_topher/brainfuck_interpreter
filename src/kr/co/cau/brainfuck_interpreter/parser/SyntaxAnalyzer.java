package kr.co.cau.brainfuck_interpreter.parser;

/**
 * Created by Christophine on 28/09/2016.
 */
public class SyntaxAnalyzer {
    private static SyntaxAnalyzer ourInstance = new SyntaxAnalyzer();

    public static SyntaxAnalyzer getInstance() {
        return ourInstance;
    }

    public static int getOpeningSquareBracketNumber(String instructions)
    {
        int bracketCount = 0;
        int index = 0;
        while (index != -1)
        {
            index =  instructions.indexOf('[', index + 1);
            ++bracketCount;
        }
        return bracketCount;
    }

    public static int getClosingSquareBracketNumber(String instructions)
    {
        int bracketCount = 0;
        int index = 0;
        while (index != -1)
        {
            index =  instructions.indexOf(']', index + 1);
            ++bracketCount;
        }
        return bracketCount;
    }
    public static boolean areInstructionsValid(String instructions)
    {
        return getClosingSquareBracketNumber(instructions) == getOpeningSquareBracketNumber(instructions);
    }
    public static int getMatchingBracketIndex(int j, String instructions)
    {
        int skipCounter = 0;
        int i = j + 1;
        while (i <= instructions.length())
        {
            if (instructions.charAt(i) == '[')
                ++skipCounter;
            if (instructions.charAt(i) == ']')
                if (skipCounter == 0)
                    return i;
                else
                    --skipCounter;
            ++i;
        }
        return -1;
    }
}
