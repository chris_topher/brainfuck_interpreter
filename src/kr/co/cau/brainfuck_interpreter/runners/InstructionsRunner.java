package kr.co.cau.brainfuck_interpreter.runners;

import kr.co.cau.brainfuck_interpreter.models.LoopStack;
import kr.co.cau.brainfuck_interpreter.models.PointerList;
import kr.co.cau.brainfuck_interpreter.parser.LexicalAnalyzer;
import kr.co.cau.brainfuck_interpreter.parser.SyntaxAnalyzer;

/**
 * Created by Christophine on 28/09/2016.
 */
public class InstructionsRunner {
    public InstructionsRunner() {

    }
    public void run(String instr) throws Exception
    {
            char[] instructions = instr.toCharArray();
            for (int j = 0; j < instructions.length; j++) {
                char command = instructions[j];
                if (!LexicalAnalyzer.isToken(command))
                    continue;
                if (LexicalAnalyzer.isOpeningLoop(command)) {
                    String loopInstructions = getLoopInstructionsFromIndex(j, instr);
                    LoopStack.getInstance().add(PointerList.getInstance().getPosition());
                    j = SyntaxAnalyzer.getMatchingBracketIndex(j, instr);
                    LoopRunner runner = new LoopRunner();
                    runner.runInstructions(loopInstructions);
                }
                this.run(command);
            }
     }
    public static String getLoopInstructionsFromIndex(int beginIndex, String instr)
    {
        int closingSquareBracketPosition = SyntaxAnalyzer.getMatchingBracketIndex(beginIndex, instr);
        String loopInstructions = instr.substring(beginIndex + 1, closingSquareBracketPosition);
        return loopInstructions;
    }
    public static void run(char command) throws Exception
    {
        switch (command)
        {
            case '+':
                PointerList.getInstance().increaseValue();
                break;
            case '-':
                PointerList.getInstance().decreaseValue();
                break;
            case '>':
                PointerList.getInstance().moveNext();
                break;
            case '<':
                PointerList.getInstance().movePrevious();
                break;
            case '.':
               System.out.print(String.format("%c", (char) PointerList.getInstance().getValue()));
                break;
            case'[':
                break;
            case ']':
                break;
        }
    }
}
