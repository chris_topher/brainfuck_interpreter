package kr.co.cau.brainfuck_interpreter.runners;

import kr.co.cau.brainfuck_interpreter.models.LoopStack;
import kr.co.cau.brainfuck_interpreter.models.PointerList;
import kr.co.cau.brainfuck_interpreter.parser.LexicalAnalyzer;
import kr.co.cau.brainfuck_interpreter.parser.SyntaxAnalyzer;

import static kr.co.cau.brainfuck_interpreter.runners.InstructionsRunner.getLoopInstructionsFromIndex;

/**
 * Created by Christophine on 30/09/2016.
 */
public class LoopRunner {
    public void runInstructions(String instr) throws Exception
    {
        int i = 0;
        int breakCondition = PointerList.getInstance().getValue(LoopStack.getInstance().top());
            while (i != breakCondition) {
                this.runCommands(instr);
                ++i;
            }
        LoopStack.getInstance().pop();
    }
    public void runCommands(String instr) throws Exception
    {
        char[] instructions = instr.toCharArray();
        for (int j = 0; j < instructions.length; j++) {
            char command = instructions[j];
            if (!LexicalAnalyzer.isToken(command))
                continue;
            if (LexicalAnalyzer.isOpeningLoop(command)) {
                String loopInstructions = getLoopInstructionsFromIndex(j, instr);
                LoopStack.getInstance().add(PointerList.getInstance().getPosition());
                j = SyntaxAnalyzer.getMatchingBracketIndex(j, instr);
                LoopRunner runner = new LoopRunner();
                runner.runInstructions(loopInstructions);
            }
            InstructionsRunner.run(command);
        }
    }
}
