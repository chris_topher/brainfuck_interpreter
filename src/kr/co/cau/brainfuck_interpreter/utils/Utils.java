package kr.co.cau.brainfuck_interpreter.utils;

/**
 * Created by Christophine on 30/09/2016.
 */
public class Utils {
    public static final short mawLoopLevel = 3;
    public static boolean isAscii(int character)
    {
        return character >= 0 && character <= 127;
    }
}
