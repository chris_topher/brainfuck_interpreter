//STUDENT ID: 50161377
++++++++++ //I set the counter to 10; This will be the break condition for the coming loop
[ // I begin the loop
>++++++     // I am moving to pointer 2; I am adding 6 to the value of this pointer
>++++++++++ // I am moving to pointer 3; I am adding 10 to the value of this pointer 
>++++++++++ // I am moving to pointer 4; I am adding 10 to the value of this pointer 
>++++++++++ // I am moving to pointer 5; I am adding 10 to the value of this pointer
>++++++++++ // I am moving to pointer 6; I am adding 10 to the value of this pointer
>++++++++++ // I am moving to pointer 7; I am adding 10 to the value of this pointer 
>++++++++++ // I am moving to pointer 8; I am adding 10 to the value of this pointer 
>++++++++++ // I am moving to pointer 9; I am adding 10 to the value of this pointer 
>++++++++++ // I am moving to pointer 10; I am adding 10 to the value of this pointer 
>++++++++++ // I am moving to pointer 11; I am adding 10 to the value of this pointer
>++++++++++ // I am moving to pointer 12; I am adding 10 to the value of this pointer 
<<<<<<<<<<< // Returning to the first pointer
-] // End of loop; Decrementing the pointer 10 times until it reaches 0 and doing the operations above 10 times

>+++++++. // Adding 7 to the value of Pointer 2; Then printing it; Ptr(2) == 60; 60 plus 7 equals 67 and corresponds to C in ASCII 
>++++. // Adding 4 to the value of Pointer 3; Then printing it; Corresponds to h in ASCII
>++++++++++++++. // Adding 14 to the value of Pointer 4; Then printing it; Corresponds to r in ASCII
>+++++. // Adding 5 to the value of Pointer 5; Then printing it; Corresponds to i in ASCII
>+++++++++++++++. // Adding 15 to the value of Pointer 5; Then printing it; Corresponds to s in ASCII
>++++++++++++++++. // Adding 16 to the value of Pointer 6; Then printing it; Corresponds to t in ASCII
>+++++++++++. // Adding 11 to the value of pointer 7; Then printing it; Corresponds to o in ASCII
>++++++++++++. // Adding 12 to the value of pointer 8; Then printing it; Corresponds to p in ASCII
>++++. // Adding 4 to the value of pointer 9; Then printing it; Corresponds to h in ASCII;
>+. // Adding 1 to the value of pointer 10; Then printing it; Corresponds to e in ASCII;
>++++++++++++++. Adding 14 to the value of pointer 11; Then printing it; Corresponds to r in Ascii
<<<<<<<<<<---------------------------------------------------------. // Returning to Pointer 2 and decrementing its value until it reaches 10 the character Line Feed in ASCII
